#include "vertex.h"

vertex::vertex(float x = 0, float y = 0, float z = 0) {
	this->x = x;
	this->y = y;
	this->z = z;
}

void vertex::addCoordinate(float x, float y, float z) {
	this->x += x;
	this->y += y;
	this->z += z;
}

float vertex::getX() {
	return this->x;
}

float vertex::getY() {
	return this->y;
}

float vertex::getZ() {
	return this->z;
}

void vertex::setX(float x) {
	this->x = x;
}

void vertex::setY(float y) {
	this->y = y;
}

void vertex::setZ(float z) {
	this->z = z;
}

void vertex::setActivate() {
	this->activate = !activate;
}

face::face(std::vector<int> points) {
	this->points = points;
}

std::vector<int> face::getPoint() {
	return this->points;
}
