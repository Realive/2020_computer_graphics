/***C/C++  http://bbs.csdn.net/topics/100091584 ***/
#include <stdio.h> 
#include <stdlib.h>
/***freeglut***/
#include <GL/freeglut.h>

#include "vertex.h"

#define SIZE 10

void setupRC();
void changeSize(int, int);
void renderScene(void);

void drawGrid();
void drawDot(std::vector<vertex>, int);
void colorDot(int, int, int);
void midpointLine(int, int, int, int);
void drawLine(std::vector<vertex>);
void drawPlane(std::vector<vertex>);
vertex searchVertex(vertex, std::vector<vertex>);

void userMouseFunc(int, int, int, int);
void mainMenu(int id);
void buildPopupMenu();

std::vector<vertex> wayPoints;
std::vector<vertex> vertices;
std::vector<vertex> planeVertices;
int numberOfLists = 0;

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(600, 80);
	glutCreateWindow("Sphere");
	
	setupRC();
	glutReshapeFunc(changeSize);
	glutDisplayFunc(renderScene);

	glutMouseFunc(userMouseFunc);

	buildPopupMenu();
	
	glutMainLoop();
	return 0;
}

void setupRC() {
	GLfloat whiteLight[] = { 0.45f, 0.45f, 0.45f, 1.0f };
	GLfloat sourceLight[] = { 0.25f, 0.25f, 0.25f, 1.0f };
	GLfloat lightPos[] = { 0.0f, 25.0f, 20.0f, 0.0f };

	glEnable(GL_LIGHTING);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, whiteLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, sourceLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, sourceLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glEnable(GL_LIGHT0);

	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);

}

void changeSize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT), 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void renderScene() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	drawGrid();
	drawDot(wayPoints, 100);

	if (wayPoints.size() > 1) {
		drawLine(wayPoints);
	}

	if (wayPoints.size() > 2) {
		drawPlane(vertices);
	}

	glutSwapBuffers();
}

void drawGrid() {
	int h = glutGet(GLUT_WINDOW_HEIGHT);
	int w = glutGet(GLUT_WINDOW_WIDTH);

	glBegin(GL_LINES);
	for (int i = 0; i < w / SIZE; i++) {
		glVertex2f(i * SIZE, 0);
		glVertex2f(i * SIZE, h);
	}
	for (int i = 0; i < h / SIZE; i++) {
		glVertex2f(0, i * SIZE);
		glVertex2f(w, i * SIZE);
	}
	glEnd();
}

void drawDot(std::vector<vertex> wp, int c) {
	std::vector<vertex>::iterator it;
	for (it = wp.begin(); it != wp.end(); it++) {	
		colorDot((*it).getX(), (*it).getY(), c);
	}
}

void colorDot(int x, int y , int c) {
	int w = glutGet(GLUT_WINDOW_WIDTH);
	int h = glutGet(GLUT_WINDOW_HEIGHT);
	float cx = (x * (255 /float(w/SIZE))+c)/255;
	float cy = (y * (255 / float(h / SIZE))+c) / 255;
	//printf("[%f, %d]", cx, 0);
	glColor3f(cx, cy, 0);
	glBegin(GL_POLYGON);

	glVertex2f(x * SIZE, y * SIZE);
	glVertex2f((x + 1) * SIZE, y * SIZE);
	glVertex2f((x + 1) * SIZE, (y + 1) * SIZE);
	glVertex2f(x * SIZE, (y + 1) * SIZE);

	glColor3f(255 / 255, 255 / 255, 255 / 255);
	glEnd();
}

void drawLine(std::vector<vertex> wp) {
	vertices.clear();
	for (int i = 0; i < wp.size(); i++) {
		vertex v = wp[i];
		vertex vp = wp[0];
		if (i + 1 != wp.size()) {
			vp = wp[i + 1];
		}
		midpointLine(v.getX(), vp.getX(), v.getY(), vp.getY());
	}
	for (int i = 0; i < vertices.size(); i++) {
		//printf("[%.0f, %.0f]", vertices[i].getX(), vertices[i].getY());
	}
	drawDot(vertices, 100);
}

void midpointLine(int x1, int x2, int y1, int y2) {
	int dx, dy, i, e;
	int incx, incy, inc1, inc2;
	int x, y;

	dx = x2 - x1;
	dy = y2 - y1;

	if (dx < 0) dx = -dx;
	if (dy < 0) dy = -dy;
	incx = 1;
	if (x2 < x1) incx = -1;
	incy = 1;
	if (y2 < y1) incy = -1;
	x = x1; y = y1;
	if (dx > dy) {
		vertices.push_back(vertex(x, y, 0));
		e = 2 * dy - dx;
		inc1 = 2 * (dy - dx);
		inc2 = 2 * dy;
		for (i = 0; i < dx; i++) {
			if (e >= 0) {
				y += incy;
				e += inc1;
			}
			else
				e += inc2;
			x += incx;
			vertices.push_back(vertex(x, y, 0));
		}

	}
	else {
		vertices.push_back(vertex(x, y, 0));
		e = 2 * dx - dy;
		inc1 = 2 * (dx - dy);
		inc2 = 2 * dx;
		for (i = 0; i < dy; i++) {
			if (e >= 0) {
				x += incx;
				e += inc1;
			}
			else
				e += inc2;
			y += incy;
			vertices.push_back(vertex(x, y, 0));
		}
	}
}

void drawPlane(std::vector<vertex> px) {
	planeVertices.clear();
	std::vector<vertex>::iterator it = px.begin();
	vertex checkPoint = vertex(-1, -1, -1);
	for (it; it != px.end(); it++) {
		vertex p = (*it);
		for (int x = p.getX(); x < glutGet(GLUT_WINDOW_WIDTH) / SIZE; x++) {
			checkPoint = searchVertex(vertex(x + 1, p.getY(), 0), px);
			if (checkPoint.getX() != -1) {
				break;
			}
		}
		for (int x = p.getX(); x < checkPoint.getX(); x++) {
			planeVertices.push_back(vertex(x, p.getY(), 0));	
		}
	}
	drawDot(planeVertices, 0);
}

vertex searchVertex(vertex v, std::vector<vertex> wp) {
	std::vector<vertex>::iterator it = wp.begin();
	for (it; it != wp.end(); it++) {
		if (v.getX() == (*it).getX() && v.getY() == (*it).getY()) {
			return vertex((*it).getX(), (*it).getY(), 0);
		}
	}
	return vertex(-1, -1, -1);
}

void userMouseFunc(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		int xi = (x / SIZE);
		int yi = (y / SIZE);
		wayPoints.push_back(vertex(xi, yi, 0));
	}
}

void buildPopupMenu() {
	glutCreateMenu(mainMenu);
	glutAddMenuEntry("Clean", 1);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void mainMenu(int id) {
	glNewList(++numberOfLists, GL_COMPILE_AND_EXECUTE);

	switch (id) {
	case 1:
		wayPoints.clear();
		vertices.clear();
		planeVertices.clear();
		break;
	default:
		break;
	}
	glutPostRedisplay();
	glEndList();
}
