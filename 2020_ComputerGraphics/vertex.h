#ifndef VERTEX_H
#define VERTEX_H

#include <vector>

class vertex {
public:
	vertex(float, float, float);
	void addCoordinate(float, float, float);
	float getX();
	float getY();
	float getZ();
	void setX(float);
	void setY(float);
	void setZ(float);
	void setActivate();
private:
	float x;
	float y;
	float z;
	bool activate = false;
};

class face {
public:
	face(std::vector<int>);
	std::vector<int> getPoint();
private:
	std::vector<int> points;
};

#endif // !VERTEX_H
#pragma once
